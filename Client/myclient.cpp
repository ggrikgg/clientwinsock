#include "stdafx.h"
#include "MyClient.h"


//MyClient::MyClient(const wchar_t ipprottype[])
//{
//	mbstowcs_s(&convertedChars, name_, name_size_, "Client", _TRUNCATE);
//	memset(&saddr_, 0, sizeof(saddr_));
//	saddr_.sin_family = AF_INET;
//	saddr_.sin_port = htons(atoi("5000"));
//	saddr_.sin_addr.s_addr = inet_addr("192.168.0.71");
//	if (!wcscmp(ipprottype, L"UDP") || !wcscmp(ipprottype, L"Udp") || !wcscmp(ipprottype, L"udp"))
//		ip_prot_type_ = 17;
//}


MyClient::MyClient(const wchar_t s_name[], const bool ipprottype)
{
	wcscpy(name_, s_name);
	memset(&saddr_, 0, sizeof(saddr_));
	saddr_.sin_family = AF_INET;
	saddr_.sin_port = htons(atoi("5000"));
	saddr_.sin_addr.s_addr = inet_addr("192.168.0.71");
	if (!ipprottype)
		ip_prot_type_ = 17;
}

MyClient::MyClient(const wchar_t s_name[], const char addr[], const char port[], const bool ipprottype)
{
	memset(&saddr_, 0, sizeof(saddr_));
	saddr_.sin_family = AF_INET;
	saddr_.sin_port = htons(atoi(port));

	saddr_.sin_addr.s_addr = inet_addr(addr);
	wcscpy(name_, s_name);

	if (!ipprottype)
		ip_prot_type_ = 17;

}

void MyClient::WSAInit()
{
	size_t err;
	WSADATA ws;
	err = WSAStartup(MAKEWORD(2, 0), &ws);

	if (err != 0)
	{
		throw WSAErr(err);
	}
	//return;
}
void MyClient::WSADeinit()
{
	WSACleanup();
}

MyClient::~MyClient()
{
}

void MyClient::SetLocate()
{
	SetConsoleCP(1251); 
	SetConsoleOutputCP(1251); 
	setlocale(LC_ALL, ".1251");
}

void MyClient::CreateandUseSocket(SOCKET& client_socket)
{
	if (client_socket == INVALID_SOCKET)
	{

		throw SocketErr(WSAGetLastError());
	}

	if (connect(client_socket, (sockaddr*)&saddr_, sizeof(saddr_)) == SOCKET_ERROR) 
	{
		shutdown(client_socket, 0);
		closesocket(client_socket);
		throw SocketErr(WSAGetLastError());
	}
}


void MyClient::RunClient()
{
	SOCKET client_socket = socket(AF_INET, SOCK_STREAM, NULL);
	SetLocate();
	CreateandUseSocket(client_socket);

	//std::vector <std::thread> th_vec;
	auto z1 = std::thread(&MyClient::Reading, this, client_socket);
	//auto z2 = std::thread(&MyClient::Writing, this, client_socket);
	Writing(client_socket);
	z1.join();
	//z2.detach();

	//th_vec.push_back(std::thread(&MyClient::Reading, this, (LPVOID)&client_socket));
	//th_vec.push_back(std::thread(&MyClient::Writing, this, (LPVOID)&client_socket));
	//for (auto& z : th_vec)
	//{
	//	z.join();
	//}

	shutdown(client_socket, 0);
	closesocket(client_socket);

	return;
}

void MyClient::Reading(size_t pinfo)
{
	SOCKET sock = pinfo;
	wchar_t Mess[mess_size_];

	int i = 0;
	while (true)
	{
		wmemset(Mess, 0, mess_size_);
		if (recv(sock, (char*)Mess, mess_size_ * sizeof(wchar_t), 0) == -1)
			ExitThread(1);
		wprintf(Mess);
		if (wcscmp(Mess, L"�����") == 0)
			ExitThread(1);
	}
}

void MyClient::Writing(size_t pinfo)
{
	SOCKET sock = pinfo;
	send(sock, (char*)name_, name_size_ * sizeof(wchar_t), 0);
	
	while (true)
	{

		std::wcin.clear();
		wchar_t Mess[mess_size_];
		wmemset(Mess, 0, mess_size_);
		wchar_t c;

		std::wcin.getline(Mess, 506);

		send(sock, (char*)Mess, mess_size_ * sizeof(wchar_t), 0);
		if (wcscmp(Mess, L"�����") == 0)
			ExitThread(1);
	}
}
