#include "stdafx.h"
#include "myclient.h"




int main()
{
	try
	{
		MyClient::WSAInit();
	}
	catch (WSAErr& e)
	{
		std::wcout << L"WSAStartup failed with error :" << e.GetErrorNum() << std::endl;
		exit(-1);
	}


	try
	{
		MyClient a(L"first", L"UDP");
		//	myclient a(L"sec","192.168.0.71","5000");
		//	myclient a;
		a.RunClient();
	}
	catch (SocketErr& e)
	{
		std::wcout << L"Creating socket error with number:" << e.GetErrorNum() << std::endl;
	}
	catch (BindErr& e)
	{
		std::wcout << L"Binding error with number:" << e.GetErrorNum() << std::endl;
	}
	catch (ListenErr& e)
	{
		std::wcout << L"Listening error with number:" << e.GetErrorNum() << std::endl;
	}
	catch (const std::exception& e)
	{
		std::cout << e.what() << "\n";
	}

	MyClient::WSADeinit();

	return 0;
}