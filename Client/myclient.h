#include <winsock2.h>
#include <iostream>
#include <thread>
#include <locale>

class MyException : public std::exception
{
public:
	typedef std::basic_string<wchar_t> wstring;
	MyException(size_t i) :err_num_(i) {}
	inline size_t GetErrorNum() { return err_num_; }
private:
	size_t err_num_ = 0;
};
class WSAErr : public MyException
{
public:
	WSAErr(size_t i) : MyException(i) {}
};

class BindErr : public MyException
{
public:
	BindErr(size_t i) : MyException(i) {}
};

class ListenErr : public MyException
{
public:
	ListenErr(size_t i) : MyException(i) {}
};
class SocketErr : public MyException
{
public:
	SocketErr(size_t i) : MyException(i) {}
};
class ConnectErr : public MyException
{
public:
	ConnectErr(size_t i) : MyException(i) {}
};

class MyClient
{
public:


//	MyClient(const wchar_t ipprottype[] = L"TCP");
	MyClient(const wchar_t s_name[], const bool);
	MyClient(const wchar_t s_name[], const char addr[], const char port[], const bool);

	~MyClient();

	void RunClient();
	static void WSAInit();
	static void WSADeinit();
private:

	sockaddr_in saddr_;
	static const size_t name_size_ = 32;
	static const size_t mess_size_ = 512;
	wchar_t name_[name_size_];
	size_t converted_chars_ = 0;
	size_t ip_prot_type_ = 6;

	void Reading(size_t pinfo);
	void Writing(size_t pinfo);
	void SetLocate();
	void CreateandUseSocket(SOCKET& client_socket);

};
